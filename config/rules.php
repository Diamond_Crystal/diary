<?php

return [
    '/' => 'site/index',
    'about' => 'site/about',
    'login' => 'auth/login',
    'logout' => 'auth/logout',
];
